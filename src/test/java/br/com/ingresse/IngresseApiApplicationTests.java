package br.com.ingresse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IngresseApiApplication.class)
@TestPropertySource(locations="classpath:test.properties")
public class IngresseApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
