package br.com.ingresse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngresseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngresseApiApplication.class, args);
	}

}
